import Converter.Acronym;

/**
 * Main application. Converts a phrase to an acronym
 */
public class Application
{

    public static void main(String[] args)
    {
        System.out.println(new Acronym(args[0]).get());
    }
}
