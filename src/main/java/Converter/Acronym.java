package Converter;

import Utilities.StringManipulation;

/** Converts a phrase into Acronym
 */
public class Acronym
{

    private String phrase;
    private String acronym = "";
    private final String [] PUNCTUATIONS = {",", "-", ":"};

    public Acronym(String phrase)
    {
        this.phrase = StringManipulation.removeStringContent(phrase, PUNCTUATIONS);

        convertToAcronym();

    }

    //converts the phrase to acronym
    private void convertToAcronym()
    {
        //split phrase into separate sections
        String [] splitPhrase = this.phrase.split("\\s+");

        for(String split : splitPhrase)
        {
            acronym = new StringBuilder(acronym).append(identifyAcronym(split)).toString();
        }
    }

    //Identifies the acronym(s) for a given string
    private String identifyAcronym(final String content)
    {
        //First rule: First character is part of the final acronym
        String partialAcronym = content.substring(0,1).toUpperCase(); //the first character is always part of the final acronym

        //second rule -- CamelCasing, the caps are part of the final acronym in the given string. However, capital letters that follow each other are not
        String [] splitCharacters = content.split("");
        boolean letterIsLowerCaseStatus = false;
        for(int startingPosition=1; startingPosition<splitCharacters.length; startingPosition++) //start at position 1 since we extracted first character
        {
            if(splitCharacters[startingPosition].matches("[A-Z]")) //if uppercase character currently and lowercase character has existed previously
            {
                if(letterIsLowerCaseStatus==true)//lower case must have existed otherwise skip
                {
                    partialAcronym = partialAcronym.concat(splitCharacters[startingPosition].toUpperCase()); //String is immutable
                    letterIsLowerCaseStatus = false;
                }
            }
            else //lowercase character
            {
                letterIsLowerCaseStatus = true;
            }
        }
        return partialAcronym;
    }



    //get the completed acronym
    public String get()
    {
        return acronym;
    }



}
