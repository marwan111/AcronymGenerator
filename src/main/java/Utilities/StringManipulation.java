package Utilities;

/**
 * Methods used to alter Strings
 */
public class StringManipulation
{

    //remove contents from a given String
    public static String removeStringContent(String currentString, final String  ... valuesToRemove)
    {
        String newString = currentString;

        for(String valueToRemove : valuesToRemove)
        {
            newString = newString.replace(valueToRemove," ");
        }

        return newString;
    }

}
